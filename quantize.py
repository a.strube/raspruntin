from argparse import ArgumentParser
import time

import numpy as np
import torch as th
import torch.quantization as qtz

import dnnlib
import legacy


def parse_args():
    parser = ArgumentParser()

    parser.add_argument(
        '--model_path',
        type=str,
        required=True,
        help='Model checkpoint to prune (a .pkl file).',
    )
    parser.add_argument(
        '--seed',
        type=int,
        default=0,
        help='Base random number generator seed.',
    )

    args = parser.parse_args()
    return args


def load_model(path, device):
    with dnnlib.util.open_url(path) as fp:
        G = legacy.load_network_pkl(fp)['G_ema'] \
                  .requires_grad_(False) \
                  .to(device) # type: ignore
    return G


def main():
    args = parse_args()

    device = th.device('cuda' if th.cuda.is_available() else 'cpu')
    np.random.seed(args.seed)
    th.manual_seed(args.seed)

    model = load_model(args.model_path, device)

    start_time = time.perf_counter()
    print('starting quantization...')

    backend = 'qnnpack'
    model.qconfig = qtz.get_default_qconfig(backend)

    th.backends.quantized.engine = backend
    model_static_quantized = qtz.prepare(model, inplace=False)
    model_static_quantized = qtz.convert(model_static_quantized, inplace=False)
    print('finished quantization after', time.perf_counter() - start_time,
          'seconds')

    th.save(model_static_quantized, 'model_quantized.pt')
    print('done')


if __name__ == '__main__':
    main()
