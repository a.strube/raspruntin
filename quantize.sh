#!/bin/bash
#SBATCH --account=hai_raspi
#SBATCH --job-name=pytorch_quantization
#SBATCH --mail-user=a.strube@fz-juelich.de
#SBATCH --mail-type=NONE
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
#SBATCH --cpus-per-task=48
#SBATCH --time=00:30:00
#SBATCH --output=quantize-%j.out
#SBATCH --partition=booster
#SBATCH --gres=gpu:4


module load GCC PyTorch tqdm Pillow-SIMD Ninja-Python
export PYTHONPATH="$(realpath ../stylegan2-ada-pytorch):$PYTHONPATH"
srun python quantize.py --model_path ../models/2366a0cffcb890fdb0ee0a193f4e0440_https___nvlabs-fi-cdn.nvidia.com_stylegan2-ada-pytorch_pretrained_ffhq.pkl
